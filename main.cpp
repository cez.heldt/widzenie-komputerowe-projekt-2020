#include <opencv2/opencv.hpp>

#include <iostream>
#include <conio.h>

#define CVUI_IMPLEMENTATION
#include <cvui.h>
#include <EnhancedWindow.h>

using namespace cv;
using namespace std;

//background substractor parameters
int history = 150, nMixtures = 100;

Ptr<BackgroundSubtractor> bs = createBackgroundSubtractorMOG2(history, nMixtures, true);

//program options
bool useHog = false;
bool useHaar = false;
bool useHumanDetection = false;
bool pause = false;

CascadeClassifier body_classifier;

HOGDescriptor hog;

vector<vector<Point>> contours;

int counter = 0;
int lineX = 200;

Mat image, fgMask, imageGray;
vector<double> areas;

void detect() {
	bs->apply(image, fgMask);

	equalizeHist(imageGray, imageGray);

	morphologyEx(fgMask, fgMask, MORPH_OPEN, MORPH_RECT, Point(-1, -1), 3);
	morphologyEx(fgMask, fgMask, MORPH_CLOSE, MORPH_RECT, Point(-1, -1), 3);

	findContours(fgMask, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);



	if (!contours.empty()) {
		for (int i = 0; i < contours.size(); i++) {
			areas.push_back(contourArea(contours[i]));
		}
	}

	for (int i = 0; i < contours.size(); i++) {
		if (contourArea(contours[i]) > 300) {
			Moments m = moments(contours[i]);
			Point center = Point(m.m10 / m.m00, m.m01 / m.m00);
			circle(image, center, 1, Scalar(0, 0, 255), 2);
			Rect bounding = boundingRect(contours[i]);
			ostringstream oss;

			vector<Rect> people;

			if ((center.x > lineX - 3 && center.x < lineX + 3)) {
				if (useHog) {
					hog.detectMultiScale(imageGray, people);
				}
				else if (useHaar)
					body_classifier.detectMultiScale(imageGray, people, 1.05);
				if (useHumanDetection && (useHog || useHaar)) {
					for (auto& person : people) {
						rectangle(imageGray, person, Scalar(0, 0, 255));
						counter++;
					}
				}
				else {
					counter++;
				}
			}
			putText(image, oss.str(), center, FONT_HERSHEY_PLAIN, 1.0, Scalar(255, 0, 0));
			rectangle(image, bounding, Scalar(255, 0, 0));
		}
	}
}

//user interface
void buildWindow(Mat frame, Mat image) {
	cvui::beginRow(frame, 10, 20, 758, 480, 30);
		cvui::image(image);
		cvui::beginColumn(128, 480, 15);
		cvui::text("Ustawienia", 2 * cvui::DEFAULT_FONT_SCALE);
		cvui::checkbox("Uzyj wykrywania ludzi", &useHumanDetection);
		cvui::text(!useHog && !useHaar ? "Wybierz algorytm" : "");
			cvui::beginRow();
			if (cvui::button(useHog ? "HOG*" : "HOG")) {
				useHog = true;
				useHaar = false;
			}
			cvui::space(std::lround(20));
			if (cvui::button(useHaar ? "Haar*" : "Haar")) {
				useHog = false;
				useHaar = true;
			}
			cvui::endRow();
		if (cvui::button(pause ? "Play" : "Playing")) {
		pause = false;
		}
		if (cvui::button(pause ? "Paused" : "Pause")) {
			pause = true;
		}
		cvui::endColumn();
	cvui::endRow();
	cvui::beginRow(frame, 10, 420, 758, 270);
		cvui::beginColumn(758, 270, 30);
			cvui::printf(2 * cvui::DEFAULT_FONT_SCALE, 13553358U, "Ilosc ludzi: %d", counter);
			cvui::text("Pozycja linii", 2 * cvui::DEFAULT_FONT_SCALE);
			cvui::trackbar(720, &lineX, 0, 640);
		cvui::endColumn();
	cvui::endRow();
}

int main(int argc, char* argv[]) {

	VideoCapture capture;

	//load classifiers for both methods
	body_classifier.load("haarcascade_fullbody.xml");
	hog.getDefaultPeopleDetector();

	if (argc == 1)
		if (!capture.open(0)) {
			cout << "Nie znaleziono kamery";
			return -1;
		}
	if (argc > 1) {
		cout << endl << argv[1] << endl;
		if (!capture.open(argv[1])) {
			cout << "Nie znaleziono pliku";
			return -1;
		}

	}

	Mat frame(600, 900, CV_8UC3);

	cvui::init("Pedestrian detection");


	//settings window parameters
	const int settingsPosX = 665;
	const int settingsPosY = 230;
	const int settingsWidth = 200;
	const int settingsHeight = 180;

	EnhancedWindow settings(settingsPosX, settingsPosY, settingsWidth, settingsHeight, "Ustawienia MOG");
	settings.minimize();

	while (true) {

		frame = Scalar(49, 52, 49);


		if (!pause) {
			capture >> image;

			if (image.empty())
				break;

			cvtColor(image, imageGray, COLOR_BGR2GRAY);

			detect();

		}



		line(image, Point(lineX, 380), Point(lineX, 95), Scalar(255, 255, 255), 6);

		buildWindow(frame, image);

		settings.begin(frame);
		if (!settings.isMinimized()) {
			cvui::text("Ilosc mieszanin", cvui::DEFAULT_FONT_SCALE);
			cvui::space(std::lround(10));
			if (cvui::trackbar(settings.width() - std::lround(20), &nMixtures, 5, 150, 1, "%.1Lf", 0, 1, cvui::DEFAULT_FONT_SCALE))
				bs = createBackgroundSubtractorMOG2(history, nMixtures, true);
			cvui::space(std::lround(10));
			cvui::text("Historia", cvui::DEFAULT_FONT_SCALE);
			cvui::space(std::lround(10));
			if (cvui::trackbar(settings.width() - std::lround(20), &history, 20, 200))
				bs = createBackgroundSubtractorMOG2(history, nMixtures, true);
		}
		settings.end();

		cvui::imshow("Pedestrian detection", frame);

		int option = waitKey(15);

		if (option == 27) {
			break;
		}

		areas.clear();

	}
	destroyAllWindows();
	capture.release();
	fgMask.release();
	image.release();
	imageGray.release();
	return 0;
}